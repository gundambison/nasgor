<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mars_theme_core
 *
 * @author R700
 * CI_Controller
 */
class Mars_theme extends CI_Controller {

    //put your code here
    protected $params;
    private $config=FALSE;

    public function __contruct() {
        parent::__construct();
    }

    function __destruct() {
        $CI = & get_instance();
        $uri=array();
        for($i=1;$i<5;$i++){
            $uri[$i]=$CI->uri->segment($i,"index");
        }
        //print_r($uri);die;
        if(! $this->config){
            $header=$uri[1]."/".$uri[2];
            $page=$uri[3];
            //4..5 biasanya id atau lainnya
            $this->config($header,$page);
        }
        
        $txt = $this->load->view('mars_tmp', $this->params,TRUE);
        echo  $txt; 
        
    }
    
    function config($header,$page='index'){
		$config_file = APPPATH . "config" . DIRECTORY_SEPARATOR .$header."_config.php";
		if(!is_file($config_file)){
			return FALSE;
		}
		
        $configs = config_load($page,$header."_config");
        foreach($configs as $key=>$value){
            $this->params[$key]=$value;
        }
        $this->config =TRUE;
    }

}
