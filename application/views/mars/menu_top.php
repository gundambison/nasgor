<div class="page-header page-header-blue">
    <div class="header-links hidden-xs">
        <a href="notifications.html"><i class="icon-comments"></i> User Alerts</a>
        <a href="#"><i class="icon-cog"></i> Settings</a>
        <a href="#"><i class="icon-signout"></i> Logout</a>
    </div>
    <h1><i class="icon-bar-chart"></i> Table Example</h1>
</div>