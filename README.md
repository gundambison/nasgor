NASGOR CI

Mengingat membuat FW Nasgor butuh waktu lama dan saya kendala dengan pekerjaan. Jadi projeknya sambil jalan dengan memperbaiki yang sudah ada.

CI Nasgor adalah CI yang memakai konsep gabung-gabung. Disini saya memakai konsep config untuk menampilkan.
controller akan dipakai khusus untuk yang bersifat posting data. Namun ini masih harus banyak perbaikan

Disini yang saya gunakan adalah membaca config dan memakai sebagai dasar dari parameter untuk view.

Saat pertama kali membuka, saya akan membuka halaman 'Mars'. Namun sangat disayangkan, saya memakai template berbayar yang sebenarnya tidak boleh saya share. Jadi terpaksa saya harus membuat ulang dengan bentuk yang lebih sederhana. Namun inti dari tulisan ini adalah saya membuat FW yang mana config digunakan untuk parameter view.

Kedepannya meminimalkan controllers